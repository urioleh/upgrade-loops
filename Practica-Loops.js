"use strict";

// **Iteración #1: Usa includes**

// Haz un bucle y muestra por consola todos aquellos valores del array que incluyan la palabra "Camiseta". 
// Usa la función .***includes*** de javascript.

const products = [
    'Camiseta de Pokemon', 
    'Pantalón coquinero',
    'Gorra de gansta',
    'Camiseta de Basket',
    'Cinrurón de Orión', 
    'AC/DC Camiseta']

let Camisetas = []
    products.forEach(element => {
       
        if (element.includes("Camiseta")) {
            Camisetas.push(element);
            
        }
        
    });
console.log(Camisetas)
// products.forEach(element => {
//         if (element.includes("Camiseta")) {
//             console.log(element)
//         }
        
//     });


// **Iteración #2: Condicionales avanzados**

// Comprueba en cada uno de los usuarios que tenga al menos dos trimestres aprobados y añade la 
// propiedad ***isApproved*** a true o false en consecuencia. Una vez lo tengas compruébalo con un 
// ***console.log***.

// Puedes usar este array para probar tu función:

const alumns = [
    {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, 
	{name: 'Lucia Aranda', T1: true, T2: false, T3: true},
	{name: 'Juan Miranda', T1: false, T2: true, T3: true},
	{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
	{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]

let aprovedAlumns = (param) =>{

for (let i = 0; i < param.length; i++) {
    const alumn = param[i];
    
    if (alumn.T1 && alumn.T2) {
        alumn.isApproved = true
    } else  if (alumn.T2 && alumn.T3) {  
        alumn.isApproved = true
    } else  if (alumn.T1 && alumn.T3) {
        alumn.isApproved = true
    } else {
        alumn.isApproved = false
    }
}
    return param
}

var res = aprovedAlumns(alumns)
console.log(res)

// ------------------------------------------------------------------------------------------------

// **Iteración #3: Probando For...of**

// Usa un bucle forof para recorrer todos los destinos del array. 
// Imprime en un ***console.log*** sus valores.

// Puedes usar este array:

const placesToTravel = ['Japon', 'Venecia', 'Murcia', 'Santander', 'Filipinas', 'Madagascar']

for (const places of placesToTravel) {
    console.log(places)
}

// ----------------------------------------------------------------------------------------------------------------------

// **Iteración #4: Probando For...in**

// Usa un **for...in** para imprimir por consola los datos del alienígena.. Puedes usar este objeto:

const alien = {
    name: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg'
}

for (const fact in alien) {
    if (Object.hasOwnProperty.call(alien, fact)) {
        const line = alien[fact];

        console.log(fact + " de Alien és: " + line + ".")
    }
    
}


// ---------------------------------------------------------------------------------------------------

// **Iteración #5: Probando For**

// Usa un bucle **for** para recorrer todos los destinos del array y elimina
// los elementos que tengan el id 11 y 40.
// Imprime en un ***console log*** el array. Puedes usar este array:

const placesToTrip = [
{id: 5, name: 'Japan'}, 
{id: 11, name: 'Venecia'},
{id: 23, name: 'Murcia'}, 
{id: 40, name: 'Santander'},
{id: 44, name: 'Filipinas'},
{id: 59, name: 'Madagascar'}]

for (let i = 0; i < placesToTrip.length; i++) {
    const element =placesToTrip[i];
    if (element.id === 11 || element.id ===40 ) {
       placesToTrip.splice(i,1)
    }
}
console.log(placesToTrip)


// -----------------------------------------------------------------------------------------------
// **Iteración #6: Mixed For...of e includes**

// Usa un bucle **for...of** para recorrer todos los juguetes y elimina los que incluyan la palabra gato. 
// Recuerda que puedes usar la función ***.includes()*** para comprobarlo.Puedes usar este array:

const toys = [
{id: 5, name: 'Buzz MyYear'}, 
{id: 11, name: 'Action Woman'}, 
{id: 23, name: 'Barbie Man'}, 
{id: 40, name: 'El gato con Guantes'},
{id: 40, name: 'El gato felix'}
]

for (const value of toys) {
    if (!value.name.includes("gato")) {
        console.log(value)
    }
    
}


// -------------------------------------------------------------------------------------------------

// **Iteración #7: For...of avanzado**

// Usa un bucle **for...of** para recorrer todos los juguetes y añade 
// los que tengan más de 15 ventas (sellCount) al array popularToys.
//  Imprimelo por consola.. Puedes usar este array:

const popularToys = [];
const juguetes = [
	{id: 5, name: 'Buzz MyYear', sellCount: 10}, 
	{id: 11, name: 'Action Woman', sellCount: 24}, 
	{id: 23, name: 'Barbie Man', sellCount: 15}, 
	{id: 40, name: 'El gato con Guantes', sellCount: 8},
	{id: 40, name: 'El gato felix', sellCount: 35}
]

for (const valor of juguetes) {
    if (valor.sellCount > 15) {
        popularToys.push(valor)
    }
}
console.log(popularToys)

